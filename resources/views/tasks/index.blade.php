@extends('main')

@section('title', '| To-Do Manager')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>To-Do Manager</h1>
		</div>
		<br>
		<div class="col-md-2">
			<a href="{{ route('tasks.create') }}" class="btn btn-primary btn-block btn-lg btn-h1-spacing">New To-Do</a>
		</div>
	</div> <!-- end of .row -->

	<div >
			Order by:
		<a  href="/tasks/?sort=asc">Ascending</a>
		|
		<a  href="/tasks/?sort=desc">Descending</a>

	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Image</th>
					<th>Title</th>
					<th>Body</th>
					<th>Tags</th>
					<th>Created at</th>
					<th>Action</th>
				</thead>
				<tbody>
					@foreach ($tasks as $task)
						<tr>
							<th>{{ $task->id }}</th>
							<th><img src="{{ asset('images/' . $task->image) }}" height="40" width="40"/></th>
							<td>{{ $task->title }}</td>
							<td>{{ substr(strip_tags($task->body), 0, 50) }}{{ strlen(strip_tags($task->body)) > 50? "...":"" }}</td>
							<td>
								@foreach ($task->tags as $tag)
									<span class="label label-default">{{ $tag->name }}</span>
								@endforeach
							</td>
							<td>{{ date('M j, Y', strtotime($task->created_at)) }}</td>
							<td>
								<a href="{{ route('tasks.show', $task->id) }}" class="btn btn-sm btn-default">View</a>
								<a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-sm btn-default">Edit</a>
							</td>
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{{	$tasks->links() }}
			</div>
		</div>
	</div> <!-- end of .row -->
@stop