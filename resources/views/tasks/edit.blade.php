@extends('main')

@section('title', '| Edit To-Do')

@section('content')

	<div class="row">
		{!! Form::model($task, ['route' => ['tasks.update', $task->id], 'method' => 'PUT', 'files' => true]) !!}
			<div class="col-md-8">
				{{ Form::label('title', 'Title:')}}
				{{ Form::text('title', null, ['class' => 'form-control input-lg']) }}

				{{ Form::label('body', 'Report:', ['class' => 'form-spacing-top'])}}
				{{ Form::textarea('body', null, ['class' => 'form-control']) }}

				{{ Form::label('tags', 'Tags:') }}
				{{ Form::select('tags[]', $tags, null, ['id' => 'tags', 'class' => 'form-control', 'multiple']) }}

				{{ Form::label('task_image', 'Update Image:', ['class' => 'form-spacing-top']) }}
					{{ Form::file('task_image') }}
			</div>

			<div class="col-md-4">
				<div class="well">
					<dl class="dl-horizontal">
						<dt>Created at:</dt>
						<dd>{{ date('M j, Y h:ia', strtotime($task->created_at)) }}</dd>
					</dl>

					<dl class="dl-horizontal">
						<dt>Updated at:</dt>
						<dd>{{ date('M j, Y h:ia', strtotime($task->updated_at)) }}</dd>
					</dl>
					<hr>
					<div class="row">
						<div class="col-md-6">
							{!! Html::linkRoute('tasks.index', 'Cancel', array($task->id), array('class' =>
							'btn btn-danger btn-block')) !!}
						</div>
						<div class="col-md-6">
							{{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
						</div>
					</div> <!-- end of .row -->
				</div>
			</div>

		{!! Form::close() !!}

	</div> <!-- end of .row -->

@stop

@section('footer')

	<script>
		$('#tags').select2({
			placeholder: 'Choose a tag'
		});
	</script>

@endsection
