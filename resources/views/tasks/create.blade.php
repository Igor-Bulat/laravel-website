@extends('main')

@section('title', '| Create New To-Do')

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Create New To-Do</h1>
            <hr>

            {{--form for adding new tasks--}}
            {!! Form::open(array('route' => 'tasks.store', 'data-parsley-validate' => '', 'files' => true)) !!}
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

            {{--select box cu taguri--}}
            {{ Form::label('tags', 'Tags:') }}
            {{ Form::select('tags[]', $tags, null, ['id' => 'tags', 'class' => 'form-control', 'multiple']) }}

            {{ Form::label('task_image', 'Upload Image:', ['class' => 'form-spacing-top']) }}
            {{ Form::file('task_image') }}

            {{ Form::label('body', 'Write a report:', ['class' => 'form-spacing-top']) }}
            {{ Form::textarea('body', null, array('class' => 'form-control', 'required' => '')) }}

            {{ Form::submit('Create', array('class' => 'btn btn-success btn-lg btn-block',
            'style' => 'margin-top: 20px')) }}
            {!! Form::close() !!}

        </div>
    </div> <!-- end of .row -->

@endsection

@section('footer')

    <script>
        $('#tags').select2({
            placeholder: 'Choose a tag'
        });
    </script>

@endsection


