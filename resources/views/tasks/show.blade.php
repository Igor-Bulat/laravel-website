@extends('main')

@section('title', "| To-Do Page")

@section('content')

    <div class="row">
        <div class="col-md-8">
            <img src="{{ asset('images/' . $tasks->image) }}"/>

            <h1>{{ $tasks->title }}</h1>

            <p class="lead">{{ $tasks->body }}</p>

            <div class="tags">
                @foreach ($tasks->tags as $tag)
                    <span class="label label-default">{{ $tag->name }}</span>
                @endforeach
            </div>
        </div>
        <div class="col-md-4">
            <div class="well">
                <dl class="dl-horizontal">
                <dt>Created At:</dt>
                <dd>{{ date('M j, Y H:ia', strtotime($tasks->created_at)) }}</dd>
                </dl>

                <dl class="dl-horizontal">
                    <dt>Last Updated:</dt>
                    <dd>{{ date('M j, Y H:ia', strtotime($tasks->updated_at)) }}</dd>
                </dl>

                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::open(['route' => ['tasks.edit', $tasks->id], 'method' => 'GET']) !!}

                        {!! Form::submit('Edit', ['class' => 'btn btn-primary btn-block']) !!}

                        {!! Form::close() !!}

                    </div>
                    <div class="col-sm-6">
                        {!! Form::open(['route' => ['tasks.destroy', $tasks->id], 'method' => 'DELETE']) !!}

                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                        {!! Form::close() !!}
                    </div>
                </div> <!-- end of .row -->

                <div class="row">
                    <div class="col-md-12">
                        {{ Html::linkRoute('tasks.index', '<< See All Posts', array(), ['class' =>
                        'btn btn-default btn-block btn-h1-spacing']) }}
                    </div>
                </div> <!-- end of .row -->
            </div>
        </div>
    </div> <!-- end of .row -->

@endsection