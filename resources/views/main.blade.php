<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials._head')
  </head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />


  <body>

    @include('partials._nav')

    <div class="container">
      @include('partials._messages')

        @yield('content')

      @include('partials._footer')

      </div> <!-- end of .container -->

      @include('partials._javascript')

        @yield('scripts')

    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        @yield('footer')

  </body>
</html>
