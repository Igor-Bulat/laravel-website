<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
  /**
   * Get the tags associate with the given task.
   * 
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
    public function tags()
    {
      return $this->belongsToMany('App\Tag')->withTimestamps();
    }
}
