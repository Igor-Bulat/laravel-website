<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  /**
   * Get the tasks associated with the given tag.
   * 
   * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
   */
    public function tasks()
    {
      return $this->belongsToMany('App\Task')->withTimestamps();
    }
}
