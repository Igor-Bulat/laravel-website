<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task;
use App\Tag;
use Session;
use Image;
use Storage;

class TaskController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->has('sort')){
            $task = Task::orderBy('id', request('sort'))
                ->paginate(5)
                ->appends('sort', request('sort'));
        } else {
            $task = Task::paginate(5);
        }

        return view('tasks.index')->withTasks($task);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // afisam toate tagurile
        $tags = Tag::pluck('name', 'id');

        return view('tasks.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the data.
        $this->validate($request, array(
          'title' => 'required|max:255',
          'body' => 'required',
          'task_image' => 'sometimes|image'
        ));

        // Store a newly created resource in database.
        $task = new Task();
        
        $task->title = $request->title;
        $task->body = $request->body;

        // Saving image for task.
        if($request->hasFile('task_image')){
            $image = $request->file('task_image');
            $filename = time() . "." . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->resize(40, 40)->save($location);
            $task->image = $filename;
        }

        $task->save();

        // Sync task with their tags.
        if (isset($request->tags)) {
            $task->tags()->sync($request->tags);
        } else {
            $task->tags()->sync(array());
        }

        // Set flash data with sussecc message.
        Session::flash('success', 'New task has been succesfully added.');

        return redirect()->route('tasks.show', $task->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);

        return view('tasks.show')->withTasks($task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Find the task in the database and save as a var.
        $task = Task::find($id);

        // Retrieves all of the values for a given key.
        $tags = Tag::pluck('name', 'id');

        // Return the view and pass in the var we previously created.
        return view('tasks.edit', compact('task', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        // Validate the data.
        $this->validate($request, array(
            'title' => 'required|max:255',
            'body' => 'required',
            'task_image' => 'image'
        ));

        $task->title = $request->input('title');
        $task->body = $request->input('body');

        // Updating image for task.
        if($request->hasFile('task_image')){
                // Add the new image
                $image = $request->file('task_image');
                $filename = time() . "." . $image->getClientOriginalExtension();
                $location = public_path('images/' . $filename);
                Image::make($image)->resize(40, 40)->save($location);
                $oldFilename = $task->image;
                $task->image = $filename;

            // Delete the old image.
                Storage::delete($oldFilename);
        }

        $task->save();

        $task->tags()->sync($request->tags);

        Session::flash('success', 'This task was successfully changed.');

        return redirect()->route('tasks.show', $task->id);
    }

    /**
     * Sort data.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sortData($id)
    {
        $tag = Tag::find($id);

        return view('tags.show')->withTag($tag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function destroy($id)
    {
        $task = Task::find($id);

        // Remove a many-to-many relationship record.
        $task->tags()->detach();

        // Deleting an image.
        Storage::delete($task->image);

        $task->delete();

        Session::flash('success', 'The task was successfully deleted.');

        return redirect()->route('tasks.index');
    }
}
